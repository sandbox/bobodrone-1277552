<?php
/**
 * @file
 * ns_bookmark.features.inc
 */

/**
 * Implementation of hook_flag_default_flags().
 */
function ns_bookmark_flag_default_flags() {
  $flags = array();
  // Exported flag: "Bookmarks".
  $flags['bookmarks'] = array(
    'content_type' => 'node',
    'title' => 'Bookmarks',
    'global' => '0',
    'types' => array(
      0 => 'ns_article',
      1 => 'ns_ch_int_group',
    ),
    'flag_short' => 'Bookmark this page',
    'flag_long' => 'Add this page to your bookmarks',
    'flag_message' => 'This page has been added to your bookmarks',
    'unflag_short' => 'Unbookmark this page',
    'unflag_long' => 'Remove this page from your bookmarks',
    'unflag_message' => 'This page has been removed from your bookmarks',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'roles' => array(
      'flag' => array(
        0 => '2',
      ),
      'unflag' => array(
        0 => '2',
      ),
    ),
    'show_on_page' => 1,
    'show_on_teaser' => 1,
    'show_on_form' => 1,
    'access_author' => '',
    'i18n' => 0,
    'api_version' => 2,
    'module' => 'ns_bookmark',
    'locked' => array(
      0 => 'name',
    ),
  );
  return $flags;
}
